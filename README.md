# Fulano

## Requisitos
* Apache / Nginx
* Composer [https://getcomposer.org/]
* Node [https://nodejs.org/es/]
* PHP >= 7.1.3
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* BCMath PHP Extension

## Instalación
1. ```git clone git@bitbucket.org:lvilabs/paywall.git```
2. ```cd fulano```
3. ```composer install```
4. ```php artisan key:generate```
5. ```npm install```
6. ```npm run dev```
```

## Hosts **/etc/hosts**
```127.0.0.1 fulano.local```
